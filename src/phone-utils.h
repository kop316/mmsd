/*
 *
 *  Multimedia Messaging Service Daemon - The Next Generation
 *
 *  Copyright (C) 2012,2013 Intel Corporation
 *                2021, Chris Talbot
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

gboolean     phone_utils_is_valid             (const char *number,
                                               const char *country_code);
gboolean     phone_utils_simple_is_valid      (const char *number);
gboolean     phone_utils_is_possible          (const char *number,
                                               const char *country_code);
gchar       *phone_utils_format_number_e164   (const char *number,
                                               const char *country_code,
                                               gboolean    return_original_number);
G_END_DECLS
